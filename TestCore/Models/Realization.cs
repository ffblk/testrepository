﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCore.Models
{
    [Table(Name = "Realization")]
    public class Realization
    {
        private int _Id;
        private int _Product_Id;
        private string _Name;
        private int _Price;
        private int _Count;
        private int _Cost;
        private DateTime _Time;
        [Column(Storage = "_Id", Name = "Id", DbType = "int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { set { _Id = value; } get { return _Id; } }
        [Column(Storage = "_Product_Id", Name = "Product_Id", DbType = "int NOT NULL")]
        public int Product_Id { set { _Product_Id = value; } get { return _Product_Id; } }
        [Column(Storage = "_Name", Name = "Name")]
        public string Name { set { _Name = value; } get { return _Name; } }
        [Column(Storage = "_Price", Name = "Price")]
        public int Price { set { _Price = value; } get { return _Price; } }
        [Column(Storage = "_Count", Name = "Count")]
        public int Count { set { _Count = value; } get { return _Count; } }
        [Column(Storage = "_Cost", Name = "Cost")]
        public int Cost { set { _Cost = value; } get { return _Cost; } }
        [Column(Storage = "_Time", Name = "Time")]
        public DateTime Time { set { _Time = value; } get { return _Time; } }

        public Realization(int id, int product_id, string name, int price, int count, int cost, DateTime time)
        {
            Id = id;
            Product_Id = product_id;
            Name = name;
            Price = price;
            Count = count;
            Cost = cost;
            Time = time;
        }
        public Realization()
        { }
    }
}
