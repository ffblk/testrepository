﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AutoMapper;
using TestCore;
using TestUI.Models;

namespace TestUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataBaseDataContext _dataContext;
        private static List<Prices> prices; 
        public HomeController()
        {
            _dataContext = new DataBaseDataContext();

        }
        public ActionResult Index()
        {
            return View(prices);
        }
        public ActionResult FindName(string findString)
        {
            prices = Mapper.Map<List<Prices>>(_dataContext.FindName(findString));
            return RedirectToAction("Index");
        }
        public ActionResult FindPrice(string findString)
        {
            int findPrice = 0;
            Int32.TryParse(findString, out findPrice);
            if(findPrice >= 0)
                prices = Mapper.Map<List<Prices>>(_dataContext.FindPrice(findPrice));
            return RedirectToAction("Index");
        }
        public ActionResult FindId(string findString)
        {
            int findId = 0;
            Int32.TryParse(findString, out findId);
            if (findId > 0)
                prices = Mapper.Map<List<Prices>>(_dataContext.FindId(findId));
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Index(List<Prices> pricesModelList)
        {
            List<Realization> array = new List<Realization>();
            foreach (var element in pricesModelList)
            {
                array.Add(new Realization(element));
            }
            _dataContext.Check(Mapper.Map<IEnumerable<TestCore.Models.Realization>>(array));
            prices = null;
            return RedirectToAction("Index");
        }
        public ActionResult CreateXml(string findString)
        {
            int findPrice = 0;
            Int32.TryParse(findString, out findPrice);
            if (findPrice>=0)
                _dataContext.WriteXml(findPrice.ToString());
            return RedirectToAction("Index");
        }
    }
}