﻿CREATE TABLE [dbo].[Prices] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (90) NOT NULL,
    [Price]  INT           NOT NULL,
    [Chosen] BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
INSERT INTO Prices ( Name, Price, Chosen) VALUES ('name1' , 100, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name2', 200, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name3', 300, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name4', 400, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name5', 500, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name6', 600, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name7', 700, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name8', 800, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name9', 900, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name10', 500, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name11', 600, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name12', 100, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name13', 200, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name14', 300, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name15', 400, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name16', 500, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name17', 600, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name18', 500, 'true');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name19', 100, 'false');
INSERT INTO Prices ( Name, Price, Chosen) VALUES ( 'name20', 200, 'true');