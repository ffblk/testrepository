﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestUI.Models
{
    public class Realization
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int Price { set; get; }
        public int Count { set; get; }
        public int Cost { set; get; }
        public DateTime Time { set; get; }
        public Realization(Prices ee)
        {
            Id = ee.Id;
            Name = ee.Name;
            Price = ee.Price;
            Count = ee.Count;
            Cost = ee.Count*ee.Price;
            Time = DateTime.UtcNow;
        }
    }
}