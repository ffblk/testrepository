﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestUI.Models
{
    public class Prices
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int Price { set; get; }
        public bool Chosen { set; get; }
        public int Count { set; get; }
    }
}